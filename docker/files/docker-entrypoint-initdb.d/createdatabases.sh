#!/usr/bin/env bash

echo 'Creating application user(s) and db(s)'

mongosh authorization-server \
    --host localhost \
    --port 27017 \
    --username $MONGODB_ROOT_USER \
    --password $MONGODB_ROOT_PASSWORD \
    --authenticationDatabase admin \
    --eval "db.createUser({user: 'authorization-server', pwd: '$MONGODB_PASSWORD', roles:[{role:'dbOwner', db: 'authorization-server'}]});"

mongosh identity-provider \
    --host localhost \
    --port 27017 \
    --username $MONGODB_ROOT_USER \
    --password $MONGODB_ROOT_PASSWORD \
    --authenticationDatabase admin \
    --eval "db.createUser({user: 'identity-provider', pwd: '$MONGODB_PASSWORD', roles:[{role:'dbOwner', db: 'identity-provider'}]});"

mongosh infrastructure-console \
    --host localhost \
    --port 27017 \
    --username $MONGODB_ROOT_USER \
    --password $MONGODB_ROOT_PASSWORD \
    --authenticationDatabase admin \
    --eval "db.createUser({user: 'infrastructure-console', pwd: '$MONGODB_PASSWORD', roles:[{role:'dbOwner', db: 'infrastructure-console'}]});"

mongosh communication-server \
    --host localhost \
    --port 27017 \
    --username $MONGODB_ROOT_USER \
    --password $MONGODB_ROOT_PASSWORD \
    --authenticationDatabase admin \
    --eval "db.createUser({user: 'communication-server', pwd: '$MONGODB_PASSWORD', roles:[{role:'dbOwner', db: 'communication-server'}]});"

mongosh test_authorization-server \
    --host localhost \
    --port 27017 \
    --username $MONGODB_ROOT_USER \
    --password $MONGODB_ROOT_PASSWORD \
    --authenticationDatabase admin \
    --eval "db.createUser({user: 'authorization-server', pwd: '$MONGODB_PASSWORD', roles:[{role:'dbOwner', db: 'test_authorization-server'}]});"
