import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
} from '@angular/router';
import { map, Observable } from 'rxjs';
import { IDTokenClaims } from '../../../interfaces/id-token-claims.interfaces';
import { ADMINISTRATOR } from '../../../constants/roles';
import { TokenService } from '../../../auth/token/token.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private token: TokenService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> {
    return this.token.loadProfile().pipe(
      map((profile: IDTokenClaims) => {
        if (profile?.roles?.includes(ADMINISTRATOR)) {
          return true;
        }
        return false;
      }),
    );
  }
}
