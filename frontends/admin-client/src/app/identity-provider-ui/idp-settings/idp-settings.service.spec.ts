import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { IdpSettingsService } from './idp-settings.service';

describe('IdpSettingsService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [],
    }),
  );

  it('should be created', () => {
    const service: IdpSettingsService = TestBed.get(IdpSettingsService);
    expect(service).toBeTruthy();
  });
});
