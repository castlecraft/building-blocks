import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../../auth/storage/storage.service';
import {
  IDENTITY_PROVIDER,
  ISSUER_URL,
  COMMUNICATION_SERVER,
  APP_URL,
} from '../../constants/storage';
import { map, switchMap } from 'rxjs/operators';
import { LOGOUT_URL } from '../../constants/url-paths';
import { TokenService } from '../../auth/token/token.service';

@Injectable({
  providedIn: 'root',
})
export class IdpSettingsService {
  constructor(
    private http: HttpClient,
    private token: TokenService,
    private storageService: StorageService,
  ) {}

  getSettings() {
    const requestUrl =
      this.storageService.getServiceURL(IDENTITY_PROVIDER) + '/settings/v1/get';
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get(requestUrl, { headers });
      }),
    );
  }

  getCloudStorages() {
    const requestUrl =
      this.storageService.getServiceURL(COMMUNICATION_SERVER) +
      '/storage/v1/list';
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<any>(requestUrl, { headers });
      }),
      map(data => data.docs),
    );
  }

  updateSettings(
    appURL: string,
    clientId: string,
    clientSecret: string,
    cloudStorageSettings: string,
  ) {
    const identityProvider =
      this.storageService.getServiceURL(IDENTITY_PROVIDER);
    const authServerURL = this.storageService.getItem(ISSUER_URL);
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(
          identityProvider + '/settings/v1/update',
          {
            appURL,
            authServerURL,
            clientId,
            clientSecret,
            cloudStorageSettings,
          },
          { headers },
        );
      }),
    );
  }

  deleteCachedTokens() {
    const identityProvider =
      this.storageService.getServiceURL(IDENTITY_PROVIDER);
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(
          identityProvider + '/settings/v1/clear_token_cache',
          {},
          { headers },
        );
      }),
    );
  }

  logout() {
    const logoutUrl =
      this.storageService.getItem(ISSUER_URL) +
      LOGOUT_URL +
      '?redirect=' +
      this.storageService.getItem(APP_URL);
    this.storageService.clearInfoLocalStorage();
    this.token.logOut();
    window.location.href = logoutUrl;
  }
}
