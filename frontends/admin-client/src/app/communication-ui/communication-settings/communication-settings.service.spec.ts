import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { CommunicationSettingsService } from './communication-settings.service';

describe('CommunicationSettingsService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [],
    }),
  );

  it('should be created', () => {
    const service: CommunicationSettingsService = TestBed.inject(
      CommunicationSettingsService,
    );
    expect(service).toBeTruthy();
  });
});
