import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../../auth/storage/storage.service';
import { ISSUER_URL, COMMUNICATION_SERVER } from '../../constants/storage';
import { map, switchMap } from 'rxjs/operators';
import { TokenService } from '../../auth/token/token.service';

@Injectable({
  providedIn: 'root',
})
export class CommunicationSettingsService {
  constructor(
    private http: HttpClient,
    private token: TokenService,
    private storageService: StorageService,
  ) {}

  getSettings() {
    const requestUrl =
      this.storageService.getServiceURL(COMMUNICATION_SERVER) +
      '/settings/v1/get';

    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get(requestUrl, { headers });
      }),
    );
  }

  getClientList() {
    const requestUrl =
      this.storageService.getItem(ISSUER_URL) + '/client/v1/trusted_clients';
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get(requestUrl, { headers });
      }),
    );
  }

  getEmailAccounts() {
    const requestUrl =
      this.storageService.getServiceURL(COMMUNICATION_SERVER) +
      '/email/v1/list';
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<any>(requestUrl, { headers });
      }),
      map(data => data.docs),
    );
  }

  getSavedSettings<T>() {
    const requestUrl =
      this.storageService.getServiceURL(COMMUNICATION_SERVER) +
      '/settings/v1/get';
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<T>(requestUrl, { headers });
      }),
    );
  }

  updateSettings(
    appURL: string,
    clientId: string,
    clientSecret: string,
    communicationServerSystemEmailAccount: string,
  ) {
    const communicationServer =
      this.storageService.getServiceURL(COMMUNICATION_SERVER);
    const authServerURL = this.storageService.getItem(ISSUER_URL);
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(
          communicationServer + '/settings/v1/update',
          {
            appURL,
            authServerURL,
            clientId,
            clientSecret,
            communicationServerSystemEmailAccount,
          },
          { headers },
        );
      }),
    );
  }
}
