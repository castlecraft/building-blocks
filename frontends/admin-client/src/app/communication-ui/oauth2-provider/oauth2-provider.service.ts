import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { switchMap } from 'rxjs';
import { StorageService } from '../../auth/storage/storage.service';
import { COMMUNICATION_SERVER } from '../../constants/storage';
import { TokenService } from '../../auth/token/token.service';

@Injectable({
  providedIn: 'root',
})
export class OAuth2ProviderService {
  constructor(
    private readonly http: HttpClient,
    private readonly storageService: StorageService,
    private readonly token: TokenService,
  ) {}

  createProvider(
    name: string,
    authServerURL: string,
    clientId: string,
    clientSecret: string,
    profileURL: string,
    tokenURL: string,
    introspectionURL: string,
    authorizationURL: string,
    revocationURL: string,
    scope: string[],
  ) {
    const url = `${this.storageService.getServiceURL(
      COMMUNICATION_SERVER,
    )}/oauth2_provider/v1/add_provider`;
    const payload = {
      name,
      authServerURL,
      clientId,
      clientSecret,
      profileURL,
      tokenURL,
      introspectionURL,
      authorizationURL,
      revocationURL,
      scope,
    };

    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, payload, {
          headers,
        });
      }),
    );
  }

  updateProvider(
    uuid: string,
    name: string,
    authServerURL: string,
    clientId: string,
    clientSecret: string,
    profileURL: string,
    tokenURL: string,
    introspectionURL: string,
    authorizationURL: string,
    revocationURL: string,
    scope: string[],
  ) {
    const url = `${this.storageService.getServiceURL(
      COMMUNICATION_SERVER,
    )}/oauth2_provider/v1/update_provider/${uuid}`;
    const payload = {
      name,
      authServerURL,
      clientId,
      clientSecret,
      profileURL,
      tokenURL,
      introspectionURL,
      authorizationURL,
      revocationURL,
      scope,
    };

    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, payload, {
          headers,
        });
      }),
    );
  }

  getProvider(uuid) {
    const url = `${this.storageService.getServiceURL(
      COMMUNICATION_SERVER,
    )}/oauth2_provider/v1/retrieve_provider/${uuid}`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<any>(url, { headers });
      }),
    );
  }

  generateRedirectURL(uuid: string) {
    return (
      this.storageService.getServiceURL(COMMUNICATION_SERVER) +
      '/oauth2_provider/callback/' +
      uuid
    );
  }
}
