import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { StorageService } from '../../auth/storage/storage.service';
import { COMMUNICATION_SERVER } from '../../constants/storage';
import { TokenService } from '../../auth/token/token.service';

@Injectable({
  providedIn: 'root',
})
export class EmailService {
  constructor(
    private readonly http: HttpClient,
    private storageService: StorageService,
    private token: TokenService,
  ) {}

  createEmail(
    name: string,
    host: string,
    port: number,
    user: string,
    pass: string,
    from: string,
  ) {
    const url = `${this.storageService.getServiceURL(
      COMMUNICATION_SERVER,
    )}/email/v1/create`;
    const emailData = {
      name,
      host,
      port,
      user,
      pass,
      from,
    };

    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, emailData, {
          headers,
        });
      }),
    );
  }

  getEmail(uuid: string): Observable<any> {
    const url = `${this.storageService.getServiceURL(
      COMMUNICATION_SERVER,
    )}/email/v1/get/${uuid}`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  getEmails() {
    const url = `${this.storageService.getServiceURL(
      COMMUNICATION_SERVER,
    )}/role/v1/find`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  updateEmail(
    uuid: string,
    name: string,
    host: string,
    port: number,
    user: string,
    pass: string,
    from: string,
  ) {
    const url = `${this.storageService.getServiceURL(
      COMMUNICATION_SERVER,
    )}/email/v1/update`;
    const emailData = {
      uuid,
      name,
      host,
      port,
      user,
      pass,
      from,
    };
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, emailData, {
          headers,
        });
      }),
    );
  }

  deleteEmailAccount(uuid: string) {
    const url = `${this.storageService.getServiceURL(
      COMMUNICATION_SERVER,
    )}/email/v1/delete/${uuid}`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, undefined, { headers });
      }),
    );
  }
}
