import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { share } from 'rxjs/operators';
import {
  APP_URL,
  CLIENT_ID,
  COMMUNICATION,
  ISSUER_URL,
  LOGIN_URL,
  REDIRECT_URI,
  SERVICES,
  SILENT_REFRESH_REDIRECT_URI,
} from '../../constants/storage';
import { AppInfoResponse } from '../token/credentials-config';
import { Storage } from './storage.utils';

export const DB_NAME = 'app_data';
export const APP_INIT = 'app_init';
export const PUT_DOC = 'put_doc';
export const GET_DOC = 'get_doc';
export const GET_ITEM = 'get_item';
export const REMOVE_ITEM = 'remove_item';
export const SET_ITEM = 'set_item';
export const SETTINGS_ID = 'settings_id';
export const CLEAR_STORAGE = 'clear_storage';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  public db = new Storage();

  private onSubject = new BehaviorSubject<{ event: string; value: any }>({
    event: APP_INIT,
    value: true,
  });
  public changes = this.onSubject.asObservable().pipe(share());

  getItem(key: string) {
    const value = this.db.get(key);
    this.onSubject.next({ event: GET_ITEM, value });
    return value;
  }

  removeItem(key: string) {
    this.onSubject.next({ event: REMOVE_ITEM, value: key });
    return this.db.remove(key);
  }

  setItem(key: string, value: string) {
    this.onSubject.next({ event: SET_ITEM, value: { key, value } });
    return this.db.set(key, value);
  }

  clear() {
    this.onSubject.next({
      event: CLEAR_STORAGE,
      value: { key: true, value: true },
    });
    return this.db.clear();
  }

  getServiceURL(serviceName: string) {
    const services = JSON.parse(this.getItem(SERVICES) || '[]');
    for (const service of services) {
      if (service.type === serviceName) {
        return service.url;
      }
    }
    return undefined;
  }

  getInfo(key: string) {
    return this.getItem(key);
  }

  clearInfoLocalStorage() {
    return this.clear();
  }

  setInfoLocalStorage(response: AppInfoResponse) {
    this.setItem(CLIENT_ID, response.clientId);
    this.setItem(REDIRECT_URI, response.callbackURLs[0]);
    this.setItem(SILENT_REFRESH_REDIRECT_URI, response.callbackURLs[1]);
    this.setItem(LOGIN_URL, response.authorizationURL);
    this.setItem(ISSUER_URL, response.authServerURL);
    this.setItem(APP_URL, response.appURL);
    this.setItem(SERVICES, JSON.stringify(response.services));
    this.setItem(COMMUNICATION, JSON.stringify(response.communication));
  }
}
