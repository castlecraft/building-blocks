import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import * as sjcl from 'sjcl';

import { StorageService } from '../storage/storage.service';
import { BearerToken } from './bearer-token.interface';
import {
  ACCESS_TOKEN,
  APP_X_WWW_FORM_URLENCODED,
  AUTHORIZATION,
  BEARER_HEADER_PREFIX,
  CODE_VERIFIER,
  CONTENT_TYPE,
  EXPIRES_IN,
  ID_TOKEN,
  LOGGED_IN,
  ONE_HOUR_IN_SECONDS_NUMBER,
  REFRESH_TOKEN,
  STATE,
  TEN_MINUTES_IN_SECONDS_NUMBER,
} from './constants';
import { AppInfoResponse, OAuth2Config } from './credentials-config';
import { Browser } from './token.utils';

@Injectable({ providedIn: 'root' })
export class TokenService {
  private iab = new Browser();
  messageUrl = '/info'; // URL to web api

  config: Observable<OAuth2Config> = this.http
    .get<AppInfoResponse>(this.messageUrl)
    .pipe(
      switchMap(appInfo => {
        return this.http
          .get<AppInfoResponse>(appInfo.authServerURL + '/info')
          .pipe(
            map(authInfo => {
              appInfo.scope = 'openid email profile roles phone';
              appInfo.services = authInfo.services;
              appInfo.enableChoosingAccount = authInfo.enableChoosingAccount;
              appInfo.communication = authInfo.communication;
              if (appInfo?.callbackURLs?.length) {
                appInfo.callbackURL = appInfo.callbackURLs[0];
              }
              this.storeConfigValues(appInfo);
              return appInfo;
            }),
          );
      }),
    );

  private headers = {
    [CONTENT_TYPE]: APP_X_WWW_FORM_URLENCODED,
  };

  constructor(
    private http: HttpClient,
    private store: StorageService,
  ) {}

  storeConfigValues(response: AppInfoResponse) {
    this.store.setInfoLocalStorage(response);
  }

  logIn() {
    this.generateAuthUrl().subscribe({
      next: url => {
        this.iab.open(url, '_self');
      },
    });
  }

  logOut() {
    const token = this.store.getItem(ACCESS_TOKEN);
    this.revokeToken(token, true);
    this.store.clear();
    this.store.setItem(LOGGED_IN, 'false');
  }

  processCode(url: string) {
    const urlParts = new URL(url);
    const query = new URLSearchParams(urlParts.searchParams);
    const code = query.get('code') as string;
    if (!code) {
      return;
    }

    const error = query.get('error');
    if (error) {
      return;
    }

    const state = query.get('state') as string;

    this.config
      .pipe(
        switchMap(config => {
          const savedState = this.store.getItem(STATE);
          const codeVerifier = this.store.getItem(CODE_VERIFIER);
          if (savedState !== state) {
            return of({ ErrorInvalidState: true });
          }
          const req = {
            grant_type: 'authorization_code',
            code,
            redirect_uri: config.callbackURL,
            client_id: config.clientId,
            scope: config.scope,
            code_verifier: codeVerifier,
          };

          return this.http.post<BearerToken>(
            config.tokenURL,
            new URLSearchParams(req).toString(),
            {
              headers: this.headers,
            },
          );
        }),
      )
      .subscribe({
        next: (response: BearerToken) => {
          const expiresIn = response.expires_in || ONE_HOUR_IN_SECONDS_NUMBER;
          const expirationTime = new Date();
          expirationTime.setSeconds(
            expirationTime.getSeconds() + Number(expiresIn),
          );

          this.store.setItem(ACCESS_TOKEN, response.access_token);

          this.saveRefreshToken(response.refresh_token);

          this.store.setItem(EXPIRES_IN, expirationTime.toISOString());
          this.store.setItem(ID_TOKEN, response.id_token);
          this.store.setItem(LOGGED_IN, 'true');
          this.store.removeItem(STATE);
          this.store.removeItem(CODE_VERIFIER);
        },
        error: err => {
          // eslint-disable-next-line no-console
          console.error({
            timestamp: new Date().toISOString(),
            ...err,
          });
        },
      });
  }

  revokeToken(accessToken: string, refresh: boolean = false) {
    this.config
      .pipe(
        switchMap(config => {
          const token = this.store.getItem(ACCESS_TOKEN);

          if (config.isAuthRequiredToRevoke) {
            this.headers[AUTHORIZATION] = BEARER_HEADER_PREFIX + accessToken;
          }
          return this.http.post(
            config.revocationURL,
            new URLSearchParams({ token }).toString(),
            {
              headers: { ...this.headers },
            },
          );
        }),
      )
      .subscribe({
        next: success => {
          if (refresh) {
            this.store.setItem(LOGGED_IN, 'false');
          }
        },
        error: error => {},
      });
  }

  getToken() {
    const expiration = this.store.getItem(EXPIRES_IN);
    const accessToken = this.store.getItem(ACCESS_TOKEN);
    if (!expiration) {
      return throwError(() => ({ InvalidExpiration: { expiration } }));
    }

    const now = new Date();
    const expirationTime = new Date(expiration);

    // expire 10 min early
    expirationTime.setSeconds(
      expirationTime.getSeconds() - TEN_MINUTES_IN_SECONDS_NUMBER,
    );
    if (now < expirationTime) {
      return of(accessToken);
    }
    return this.refreshToken();
  }

  refreshToken() {
    return this.config.pipe(
      switchMap(config => {
        const requestBody = {
          grant_type: 'refresh_token',
          refresh_token: this.getRefreshToken(),
          redirect_uri: config.callbackURL,
          client_id: config.clientId,
          scope: config.scope,
        };
        return this.http
          .post<BearerToken>(
            config.tokenURL,
            new URLSearchParams(requestBody).toString(),
            {
              headers: this.headers,
            },
          )
          .pipe(
            switchMap(bearerToken => {
              this.revokeToken(bearerToken.access_token);
              const expirationTime = new Date();
              const expiresIn =
                bearerToken.expires_in || ONE_HOUR_IN_SECONDS_NUMBER;
              expirationTime.setSeconds(
                expirationTime.getSeconds() + Number(expiresIn),
              );
              this.store.setItem(EXPIRES_IN, expirationTime.toISOString());
              this.store.setItem(ID_TOKEN, bearerToken.id_token);
              this.store.setItem(ACCESS_TOKEN, bearerToken.access_token);

              this.saveRefreshToken(bearerToken.refresh_token);
              return of(bearerToken.access_token);
            }),
          );
      }),
    );
  }

  generateAuthUrl() {
    return this.config.pipe(
      switchMap(config => {
        const state = this.generateRandomString();
        this.store.setItem(STATE, state);

        const codeVerifier = this.generateRandomString();
        this.store.setItem(CODE_VERIFIER, codeVerifier);

        const challenge = sjcl.codec.base64
          .fromBits(sjcl.hash.sha256.hash(codeVerifier))
          .replace(/\+/g, '-')
          .replace(/\//g, '_')
          .replace(/=/g, '');

        let url = config.authorizationURL;
        url += '?scope=' + encodeURIComponent(config.scope);
        url += '&response_type=code';
        url += '&client_id=' + config.clientId;
        url += '&redirect_uri=' + encodeURIComponent(config.callbackURL);
        url += '&state=' + state;
        url += '&code_challenge_method=S256';
        url += '&prompt=select_account';
        url += '&code_challenge=' + challenge;

        return of(url);
      }),
    );
  }

  generateRandomString(stateLength: number = 32) {
    let result = '';
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < stateLength; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  saveRefreshToken(refreshToken: string) {
    this.store.setItem(REFRESH_TOKEN, refreshToken);
  }

  getRefreshToken(): string {
    return this.store.getItem(REFRESH_TOKEN);
  }

  loadProfile() {
    return this.config.pipe(
      switchMap(config => {
        return this.getToken().pipe(
          switchMap(token => {
            return this.http.get<any>(config.profileURL, {
              headers: { authorization: 'Bearer ' + token },
            });
          }),
        );
      }),
    );
  }

  getHeaders() {
    return this.getToken().pipe(
      map(token => {
        return new HttpHeaders({
          [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
        });
      }),
    );
  }
}
