import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { switchMap } from 'rxjs';
import { ISSUER_URL, APP_URL } from '../../constants/storage';
import { StorageService } from '../../auth/storage/storage.service';
import { TokenService } from '../../auth/token/token.service';

@Injectable({
  providedIn: 'root',
})
export class ServiceService {
  constructor(
    private http: HttpClient,
    private token: TokenService,
    private store: StorageService,
  ) {}

  getClientList() {
    const requestUrl =
      this.store.getItem(ISSUER_URL) + '/client/v1/trusted_clients';
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get(requestUrl, { headers });
      }),
    );
  }

  getService(uuid: string) {
    const requestUrl =
      this.store.getItem(APP_URL) + '/service/v1/get_by_uuid/' + uuid;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<any>(requestUrl, { headers });
      }),
    );
  }

  createService(
    name: string,
    type: string,
    clientId: string,
    serviceURL: string,
  ) {
    const url = `${this.store.getItem(APP_URL)}/service/v1/register`;
    const serviceData = { name, type, clientId, serviceURL };
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post<any>(url, serviceData, {
          headers,
        });
      }),
    );
  }

  modifyService(
    clientId: string,
    name: string,
    type: string,
    serviceURL: string,
  ) {
    const url = `${this.store.getItem(APP_URL)}/service/v1/modify/${clientId}`;
    const serviceData = { name, type, serviceURL };
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post<any>(url, serviceData, {
          headers,
        });
      }),
    );
  }

  deleteService(clientId: string) {
    const url = `${this.store.getItem(APP_URL)}/service/v1/delete/${clientId}`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, undefined, { headers });
      }),
    );
  }
}
