import { TestBed } from '@angular/core/testing';

import { ServiceTypeService } from './service-type.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ServiceTypeService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [],
    }),
  );

  it('should be created', () => {
    const service: ServiceTypeService = TestBed.get(ServiceTypeService);
    expect(service).toBeTruthy();
  });
});
