import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, switchMap } from 'rxjs';
import { StorageService } from '../../auth/storage/storage.service';
import { APP_URL } from '../../constants/storage';
import { TokenService } from '../../auth/token/token.service';

@Injectable({
  providedIn: 'root',
})
export class ServiceTypeService {
  constructor(
    private readonly http: HttpClient,
    private storageService: StorageService,
    private token: TokenService,
  ) {}

  getServiceType(uuid: string): Observable<any> {
    const url = `${this.storageService.getInfo(
      APP_URL,
    )}/service_type/v1/get_by_uuid/${uuid}`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<any>(url, { headers });
      }),
    );
  }

  createServiceType(serviceTypeName: string) {
    const url = `${this.storageService.getInfo(
      APP_URL,
    )}/service_type/v1/create`;
    const serviceTypeData = {
      name: serviceTypeName,
    };
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, serviceTypeData, {
          headers,
        });
      }),
    );
  }

  deleteServiceType(name: string) {
    const url = `${this.storageService.getInfo(
      APP_URL,
    )}/service_type/v1/delete/${name}`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, undefined, {
          headers,
        });
      }),
    );
  }
}
