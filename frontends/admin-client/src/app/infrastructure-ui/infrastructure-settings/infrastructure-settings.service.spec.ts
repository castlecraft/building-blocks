import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { InfrastructureSettingsService } from './infrastructure-settings.service';

describe('InfrastructureSettingsService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [],
    }),
  );

  it('should be created', () => {
    const service: InfrastructureSettingsService = TestBed.get(
      InfrastructureSettingsService,
    );
    expect(service).toBeTruthy();
  });
});
