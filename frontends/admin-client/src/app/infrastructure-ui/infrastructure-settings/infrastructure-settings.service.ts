import { Injectable } from '@angular/core';
import { switchMap } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../../auth/storage/storage.service';
import { INFRASTRUCTURE_CONSOLE, ISSUER_URL } from '../../constants/storage';
import { TokenService } from '../../auth/token/token.service';

@Injectable({
  providedIn: 'root',
})
export class InfrastructureSettingsService {
  constructor(
    private http: HttpClient,
    private token: TokenService,
    private storageService: StorageService,
  ) {}

  getSettings() {
    const requestUrl =
      this.storageService.getServiceURL(INFRASTRUCTURE_CONSOLE) +
      '/settings/v1/get';
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get(requestUrl, { headers });
      }),
    );
  }

  updateSettings(appURL: string, clientId: string, clientSecret: string) {
    const infrastructureConsole = this.storageService.getServiceURL(
      INFRASTRUCTURE_CONSOLE,
    );
    const authServerURL = this.storageService.getItem(ISSUER_URL);
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(
          infrastructureConsole + '/settings/v1/update',
          {
            authServerURL,
            appURL,
            clientId,
            clientSecret,
          },
          { headers },
        );
      }),
    );
  }
}
