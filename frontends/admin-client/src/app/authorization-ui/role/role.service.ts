import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../../auth/storage/storage.service';
import { ISSUER_URL } from '../../constants/storage';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { TokenService } from '../../auth/token/token.service';

@Injectable({
  providedIn: 'root',
})
export class RoleService {
  constructor(
    private readonly http: HttpClient,
    private readonly store: StorageService,
    private readonly token: TokenService,
  ) {}

  createRole(name: string) {
    const url = `${this.store.getItem(ISSUER_URL)}/role/v1/create`;
    const clientData = { name };
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, clientData, { headers });
      }),
    );
  }

  getRole(role: string): Observable<any> {
    const url = `${this.store.getItem(ISSUER_URL)}/role/v1/${role}`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  getRoles() {
    const url = `${this.store.getItem(ISSUER_URL)}/role/v1/find`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  updateRole(uuid: string, roleName: string) {
    const url = `${this.store.getItem(ISSUER_URL)}/role/v1/update/${uuid}`;
    const roleData = {
      name: roleName,
    };
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, roleData, { headers });
      }),
    );
  }

  deleteRole(roleName: string) {
    const url = `${this.store.getItem(ISSUER_URL)}/role/v1/delete/${roleName}`;

    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, undefined, { headers });
      }),
    );
  }
}
