import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, switchMap } from 'rxjs';
import { StorageService } from '../../auth/storage/storage.service';
import { ISSUER_URL } from '../../constants/storage';
import { Scope } from './scope.interface';
import { TokenService } from '../../auth/token/token.service';

@Injectable({
  providedIn: 'root',
})
export class ScopeService {
  constructor(
    private readonly http: HttpClient,
    private readonly store: StorageService,
    private readonly token: TokenService,
  ) {}

  getScope(uuid: string): Observable<any> {
    const url = `${this.store.getItem(ISSUER_URL)}/scope/v1/${uuid}`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  updateScope(uuid: string, name: string, description: string) {
    const url = `${this.store.getItem(ISSUER_URL)}/scope/v1/update/${uuid}`;

    const userData: Scope = {
      name,
      description,
    };

    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, userData, { headers });
      }),
    );
  }

  createScope(name: string, description: string) {
    const url = `${this.store.getItem(ISSUER_URL)}/scope/v1/create`;
    const scopeData = {
      name,
      description,
    };
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, scopeData, { headers });
      }),
    );
  }

  deleteScope(scopeName: string) {
    const url = `${this.store.getItem(
      ISSUER_URL,
    )}/scope/v1/delete/${scopeName}`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, undefined, { headers });
      }),
    );
  }
}
