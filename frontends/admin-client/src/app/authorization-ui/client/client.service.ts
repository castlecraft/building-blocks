import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, switchMap } from 'rxjs';
import { StorageService } from '../../auth/storage/storage.service';
import { ISSUER_URL } from '../../constants/storage';
import { ClientAuthentication } from './client-authentication.enum';
import { TokenService } from '../../auth/token/token.service';

@Injectable()
export class ClientService {
  constructor(
    private readonly http: HttpClient,
    private readonly store: StorageService,
    private readonly token: TokenService,
  ) {}

  getClient(clientID: string): Observable<any> {
    const url = `${this.store.getItem(ISSUER_URL)}/client/v1/get/${clientID}`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  verifyClient(clientURL: string) {
    const url = `${clientURL}/info`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  createClient(
    clientName: string,
    authenticationMethod: ClientAuthentication,
    callbackURLs: string[],
    scopes: string[],
    isTrusted: string,
    autoApprove: boolean,
  ) {
    const url = `${this.store.getItem(ISSUER_URL)}/client/v1/create`;
    const clientData = {
      name: clientName,
      authenticationMethod,
      redirectUris: callbackURLs,
      allowedScopes: scopes,
      isTrusted,
      autoApprove,
    };
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, clientData, { headers });
      }),
    );
  }

  updateClient(
    clientId: string,
    clientName: string,
    authenticationMethod: ClientAuthentication,
    tokenDeleteEndpoint: string,
    userDeleteEndpoint: string,
    callbackURLs: string[],
    scopes: string[],
    isTrusted: string,
    autoApprove: boolean,
  ) {
    const url = `${this.store.getItem(
      ISSUER_URL,
    )}/client/v1/update/${clientId}`;

    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(
          url,
          {
            name: clientName,
            authenticationMethod,
            tokenDeleteEndpoint,
            userDeleteEndpoint,
            redirectUris: callbackURLs,
            allowedScopes: scopes,
            isTrusted,
            autoApprove,
          },
          { headers },
        );
      }),
    );
  }

  invokeSetup(clientURL: string, savedClient: any) {
    const payload = {
      appURL: clientURL,
      authServerURL: this.store.getItem(ISSUER_URL),
      clientId: savedClient.clientId,
      clientSecret: savedClient.clientSecret,
      callbackURLs: savedClient.redirectUris,
    };
    const url = `${clientURL}/setup`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, payload, { headers });
      }),
    );
  }

  getScopes() {
    const url = `${this.store.getItem(ISSUER_URL)}/scope/v1/find`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  deleteClient(clientId: string) {
    const url = `${this.store.getItem(
      ISSUER_URL,
    )}/client/v1/delete/${clientId}`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, undefined, { headers });
      }),
    );
  }
}
