import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { AuthSettingsService } from './auth-settings.service';
import { StorageService } from '../../auth/storage/storage.service';
import { TokenService } from '../../auth/token/token.service';
import { of } from 'rxjs';

describe('AuthSettingsService', () => {
  const token: Partial<TokenService> = {
    getToken: () => of('access_token'),
    logOut() {},
  };
  const store: Partial<StorageService> = {
    getItem: () => 'item',
    getServiceURL: () => 'service_url',
  };
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: TokenService, useValue: token },
        { provide: StorageService, useValue: store },
      ],
    }),
  );

  it('should be created', () => {
    const service: AuthSettingsService = TestBed.get(AuthSettingsService);
    expect(service).toBeTruthy();
  });
});
