import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { switchMap } from 'rxjs/operators';
import { StorageService } from '../../auth/storage/storage.service';
import { ISSUER_URL } from '../../constants/storage';
import { TokenService } from '../../auth/token/token.service';

@Injectable({
  providedIn: 'root',
})
export class SocialLoginService {
  constructor(
    private readonly http: HttpClient,
    private storageService: StorageService,
    private readonly token: TokenService,
  ) {}

  getSocialLogin(uuid: string) {
    const url = `${this.storageService.getInfo(
      ISSUER_URL,
    )}/social_login/v1/get/${uuid}`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  createSocialLogin(
    name: string,
    description: string,
    clientId: string,
    clientSecret: string,
    authorizationURL: string,
    tokenURL: string,
    introspectionURL: string,
    baseURL: string,
    profileURL: string,
    revocationURL: string,
    scope: string[],
    clientSecretToTokenEndpoint: boolean,
  ) {
    const url = `${this.storageService.getInfo(
      ISSUER_URL,
    )}/social_login/v1/create`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(
          url,
          {
            name,
            description,
            clientId,
            clientSecret,
            authorizationURL,
            tokenURL,
            introspectionURL,
            baseURL,
            profileURL,
            revocationURL,
            scope,
            clientSecretToTokenEndpoint,
          },
          { headers },
        );
      }),
    );
  }

  updateSocialLogin(
    uuid: string,
    name: string,
    description: string,
    clientId: string,
    clientSecret: string,
    authorizationURL: string,
    tokenURL: string,
    introspectionURL: string,
    baseURL: string,
    profileURL: string,
    revocationURL: string,
    scope: string[],
    clientSecretToTokenEndpoint: boolean,
  ) {
    const url = `${this.storageService.getInfo(
      ISSUER_URL,
    )}/social_login/v1/update/${uuid}`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(
          url,
          {
            name,
            description,
            clientId,
            clientSecret,
            authorizationURL,
            tokenURL,
            introspectionURL,
            baseURL,
            profileURL,
            revocationURL,
            scope,
            clientSecretToTokenEndpoint,
          },
          { headers },
        );
      }),
    );
  }

  generateRedirectURL(uuid) {
    return (
      this.storageService.getInfo(ISSUER_URL) + '/social_login/callback/' + uuid
    );
  }

  deleteSocialLogin(uuid: string) {
    const url = `${this.storageService.getInfo(
      ISSUER_URL,
    )}/social_login/v1/delete/${uuid}`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, undefined, { headers });
      }),
    );
  }
}
