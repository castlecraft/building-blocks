import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { StorageService } from '../../auth/storage/storage.service';
import { ISSUER_URL } from '../../constants/storage';
import { UserUpdate } from './user-update.interface';
import { TokenService } from '../../auth/token/token.service';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(
    private readonly http: HttpClient,
    private readonly store: StorageService,
    private readonly token: TokenService,
  ) {}

  getUser(userID: string): Observable<any> {
    const url = `${this.store.getInfo(ISSUER_URL)}/user/v1/get/${userID}`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  verifyUser(userURL: string) {
    const url = `${userURL}/info`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  createUser(
    fullName: string,
    userEmail: string,
    userPhone: number,
    userPassword: string,
    userRole: string,
  ) {
    const url = `${this.store.getInfo(ISSUER_URL)}/user/v1/create`;
    const userData = {
      name: fullName,
      email: userEmail,
      phone: userPhone,
      password: userPassword,
      roles: userRole,
    };
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, userData, { headers });
      }),
    );
  }

  updateUser(
    uuid: string,
    fullName: string,
    roles: string,
    password?: string,
    disabled = false,
  ) {
    const url = `${this.store.getInfo(ISSUER_URL)}/user/v1/update/${uuid}`;

    const userData: UserUpdate = {
      name: fullName,
      roles,
      disabled,
    };

    if (password) userData.password = password;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, userData, { headers });
      }),
    );
  }

  invokeSetup(userURL: string, savedUser: any) {
    const payload = {
      appURL: userURL,
      authServerURL: this.store.getInfo(ISSUER_URL),
      uuid: savedUser.uuid,
      name: savedUser.name,
      role: savedUser.role,
    };
    const url = `${userURL}/setup`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, payload, { headers });
      }),
    );
  }

  enablePasswordLessLogin(userUuid: string) {
    const url = `${this.store.getInfo(
      ISSUER_URL,
    )}/user/v1/enable_password_less_login`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, { userUuid }, { headers });
      }),
    );
  }

  disablePasswordLessLogin(userUuid: string) {
    const url = `${this.store.getInfo(
      ISSUER_URL,
    )}/user/v1/disable_password_less_login`;

    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, { userUuid }, { headers });
      }),
    );
  }

  getRoles() {
    const url = `${this.store.getInfo(ISSUER_URL)}/role/v1/find`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  deleteUser(userUuid: string) {
    const url = `${this.store.getInfo(ISSUER_URL)}/user/v1/delete/${userUuid}`;

    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.post(url, undefined, { headers });
      }),
    );
  }
}
