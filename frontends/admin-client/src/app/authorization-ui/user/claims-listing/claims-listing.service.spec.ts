import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { ClaimsListingService } from './claims-listing.service';

describe('ClaimsListingService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [],
    }),
  );

  it('should be created', () => {
    const service: ClaimsListingService = TestBed.get(ClaimsListingService);
    expect(service).toBeTruthy();
  });
});
