import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { ISSUER_URL } from '../../../constants/storage';
import { TokenService } from '../../../auth/token/token.service';
import { StorageService } from '../../../auth/storage/storage.service';
import { switchMap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ClaimsListingService {
  constructor(
    private token: TokenService,
    private store: StorageService,
    private http: HttpClient,
  ) {}

  findClaims(
    uuid: string,
    filter = '',
    sortOrder = 'asc',
    pageNumber = 0,
    pageSize = 10,
  ) {
    const baseUrl = this.store.getItem(ISSUER_URL);

    const url = `${baseUrl}/user_claim/v1/retrieve_user_claims?uuid=${uuid}`;

    const params = new HttpParams()
      .set('limit', pageSize.toString())
      .set('offset', (pageNumber * pageSize).toString())
      .set('search', filter)
      .set('sort', sortOrder);

    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get(url, {
          params,
          headers,
        });
      }),
    );
  }
}
