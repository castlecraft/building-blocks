import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { SharedUIModule } from './shared-ui/shared-ui.module';
import { AuthorizationUIModule } from './authorization-ui/authorization-ui.module';
import { CommunicationUIModule } from './communication-ui/communication-ui.module';
import { InfrastructureUIModule } from './infrastructure-ui/infrastructure-ui.module';
import { IdentityProviderUIModule } from './identity-provider-ui/identity-provider-ui.module';
import { CallbackPage } from './auth/callback/callback.page';

@NgModule({
  declarations: [AppComponent, CallbackPage],
  imports: [
    SharedUIModule,
    CommunicationUIModule,
    AuthorizationUIModule,
    InfrastructureUIModule,
    IdentityProviderUIModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
