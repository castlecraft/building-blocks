import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Component } from '@angular/core';

import { NavigationComponent } from './navigation.component';
import { MaterialModule } from '../../shared-imports/material/material.module';
import { NavigationService } from './navigation.service';
import { TokenService } from '../../auth/token/token.service';

describe('NavigationComponent', () => {
  let component: NavigationComponent;
  let fixture: ComponentFixture<NavigationComponent>;
  const navSvc: Partial<NavigationService> = {
    setAutoLoginInDuration() {},
  };
  const tokenSvc: Partial<TokenService> = {
    logIn() {},
    logOut() {},
  };

  @Component({ selector: 'app-home', template: '' })
  class HomeComponent {}

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, BrowserAnimationsModule, MaterialModule],
      declarations: [HomeComponent, NavigationComponent],
      providers: [
        { provide: NavigationService, useValue: navSvc },
        { provide: TokenService, useValue: tokenSvc },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(NavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeDefined();
  });
});
