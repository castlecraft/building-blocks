import { Injectable } from '@angular/core';
import {
  CLIENT_ID,
  REDIRECT_URI,
  SILENT_REFRESH_REDIRECT_URI,
  LOGIN_URL,
  ISSUER_URL,
  USER_UUID,
} from '../../constants/storage';
import { timer } from 'rxjs';
import { LOGGED_IN } from '../../auth/token/constants';
import { TokenService } from '../../auth/token/token.service';
import { StorageService } from '../../auth/storage/storage.service';
import { DURATION } from '../../constants/common';

@Injectable()
export class NavigationService {
  constructor(
    private readonly token: TokenService,
    private readonly store: StorageService,
  ) {}

  setAutoLoginInDuration() {
    timer(DURATION).subscribe(() => {
      if (this.store.getItem(LOGGED_IN) !== 'true') {
        this.token.logIn();
      }
    });
  }

  clearInfoStorage() {
    this.store.removeItem(CLIENT_ID);
    this.store.removeItem(REDIRECT_URI);
    this.store.removeItem(SILENT_REFRESH_REDIRECT_URI);
    this.store.removeItem(LOGIN_URL);
    this.store.removeItem(ISSUER_URL);
    this.store.removeItem(USER_UUID);
  }
}
