import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { NavigationEnd, Router } from '@angular/router';
import { NavigationService } from './navigation.service';
import {
  ISSUER_URL,
  APP_URL,
  USER_UUID,
  COMMUNICATION,
  IDENTITY_PROVIDER,
} from '../../constants/storage';
import { LOGOUT_URL } from '../../constants/url-paths';
import { TokenService } from '../../auth/token/token.service';
import { LOGGED_IN } from '../../auth/token/constants';
import { SET_ITEM, StorageService } from '../../auth/storage/storage.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
})
export class NavigationComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));
  tokenIsValid: boolean;
  route: string;
  loggedIn: boolean;
  isIdentityProviderAvailable: boolean;
  isCommunicationEnabled: boolean;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private navigationService: NavigationService,
    private token: TokenService,
    private store: StorageService,
  ) {}

  ngOnInit(): void {
    this.router.events
      .pipe(filter(route => route instanceof NavigationEnd))
      .subscribe((route: NavigationEnd) => {
        this.route = route.url;
      });

    const loggedIn = this.store.getItem(LOGGED_IN);
    if (loggedIn === 'true') {
      this.tokenIsValid = true;
      this.loggedIn = true;
      this.router.navigate(['/dashboard']);
    }

    this.store.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === LOGGED_IN) {
          if (res.value?.value === 'true') {
            this.loggedIn = true;
            this.tokenIsValid = true;
            this.router.navigate(['/dashboard']);
          }
        }
      },
      error: error => {},
    });

    try {
      this.isIdentityProviderAvailable =
        this.store.getServiceURL(IDENTITY_PROVIDER);
    } catch (error) {
      this.isIdentityProviderAvailable = false;
    }

    try {
      this.isCommunicationEnabled = JSON.parse(
        this.store.getItem(COMMUNICATION),
      );
    } catch (error) {
      this.isCommunicationEnabled = false;
    }

    this.setAutoLoginInDuration();
  }

  setAutoLoginInDuration() {
    this.navigationService.setAutoLoginInDuration();
  }

  login() {
    this.token.logIn();
  }

  logout() {
    const issuerURL = this.store.getItem(ISSUER_URL);
    const appURL = this.store.getItem(APP_URL);
    const logoutUrl = issuerURL + '/auth/logout?redirect=' + appURL;
    this.navigationService.clearInfoStorage();
    this.token.logOut();
    this.tokenIsValid = false;
    window.location.href = logoutUrl;
  }

  chooseAccount() {
    const appURL = this.store.getItem(APP_URL);
    window.open(appURL, '_blank', 'noreferrer=true');
  }

  logoutCurrentUser() {
    const issuerURL = this.store.getItem(ISSUER_URL);
    const userUUID = this.store.getItem(USER_UUID);
    const appURL = this.store.getItem(APP_URL);
    const logoutURL =
      issuerURL +
      LOGOUT_URL +
      '/' +
      userUUID +
      '?redirect=' +
      encodeURIComponent(appURL);
    this.store.clear();
    window.location.href = logoutURL;
  }

  addModel() {
    // TODO: make it better in UI/UX
    const routeArray = this.route.split('/');
    const index = routeArray.indexOf('list');
    if (index !== -1) routeArray[index] = 'new';
    this.router.navigate(routeArray);
  }
}
