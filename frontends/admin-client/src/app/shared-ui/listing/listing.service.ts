import { Injectable } from '@angular/core';
import { StorageService } from '../../auth/storage/storage.service';
import {
  ISSUER_URL,
  COMMUNICATION_SERVER,
  APP_URL,
} from '../../constants/storage';
import { HttpClient, HttpParams } from '@angular/common/http';
import { TokenService } from '../../auth/token/token.service';
import { switchMap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ListingService {
  constructor(
    private storageService: StorageService,
    private http: HttpClient,
    private token: TokenService,
  ) {}

  findModels(
    model: string,
    filter = '',
    sortOrder = 'asc',
    pageNumber = 0,
    pageSize = 10,
  ) {
    let baseUrl = this.storageService.getInfo(ISSUER_URL);

    if (['storage', 'email', 'oauth2_provider'].includes(model)) {
      baseUrl = this.storageService.getServiceURL(COMMUNICATION_SERVER);
    }

    if (['service', 'service_type'].includes(model)) {
      baseUrl = this.storageService.getInfo(APP_URL);
    }

    const url = `${baseUrl}/${model}/v1/list`;
    const params = new HttpParams()
      .set('limit', pageSize.toString())
      .set('offset', (pageNumber * pageSize).toString())
      .set('search', filter)
      .set('sort', sortOrder);

    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get(url, {
          params,
          headers,
        });
      }),
    );
  }
}
