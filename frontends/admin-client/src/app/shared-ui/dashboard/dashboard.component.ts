import { Component, OnInit } from '@angular/core';
import { IDTokenClaims } from '../../interfaces/id-token-claims.interfaces';
import { ADMINISTRATOR } from '../../constants/roles';
import { LOGIN_AS_ADMINISTRATOR } from '../../constants/messages';
import { TokenService } from '../../auth/token/token.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  message: string;
  constructor(private token: TokenService) {}

  ngOnInit() {
    this.token.loadProfile().subscribe((profile: IDTokenClaims) => {
      const idClaims: IDTokenClaims = profile || { roles: [] };
      if (!idClaims.roles.includes(ADMINISTRATOR)) {
        this.message = LOGIN_AS_ADMINISTRATOR;
      }
    });
  }
}
