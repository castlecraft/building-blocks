import { TestBed } from '@angular/core/testing';

import { MultifactorService } from './multifactor.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TokenService } from '../auth/token/token.service';

describe('MultifactorService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: TokenService,
          useValue: {},
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: MultifactorService = TestBed.get(MultifactorService);
    expect(service).toBeTruthy();
  });
});
