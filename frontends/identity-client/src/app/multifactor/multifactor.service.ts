import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ISSUER_URL } from '../constants/storage';
import {
  INITIALIZE_2FA,
  VERIFY_2FA,
  DISABLE_2FA,
} from '../constants/url-paths';
import { TokenService } from '../auth/token/token.service';
import { forkJoin, from, switchMap } from 'rxjs';
import { AUTHORIZATION, BEARER_HEADER_PREFIX } from '../auth/token/constants';
import { StorageService } from '../auth/storage/storage.service';

@Injectable({
  providedIn: 'root',
})
export class MultifactorService {
  constructor(
    private http: HttpClient,
    private token: TokenService,
    private store: StorageService,
  ) {}

  enable2fa() {
    return forkJoin({
      issuerURL: from(this.store.getItem(ISSUER_URL)),
      token: this.token.getToken(),
    }).pipe(
      switchMap(({ issuerURL, token }) => {
        return this.http.post(
          issuerURL + INITIALIZE_2FA + '?restart=true',
          null,
          {
            headers: {
              [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
            },
          },
        );
      }),
    );
  }

  verify2fa(otp: string) {
    return forkJoin({
      issuerURL: from(this.store.getItem(ISSUER_URL)),
      token: this.token.getToken(),
    }).pipe(
      switchMap(({ issuerURL, token }) => {
        return this.http.post(
          issuerURL + VERIFY_2FA,
          { otp },
          {
            headers: {
              [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
            },
          },
        );
      }),
    );
  }

  disable2fa() {
    return forkJoin({
      issuerURL: from(this.store.getItem(ISSUER_URL)),
      token: this.token.getToken(),
    }).pipe(
      switchMap(({ issuerURL, token }) => {
        return this.http.post(issuerURL + DISABLE_2FA, null, {
          headers: {
            [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
          },
        });
      }),
    );
  }
}
