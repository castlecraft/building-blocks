import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TokenService } from '../../auth/token/token.service';

import { CallbackPage } from './callback.page';

describe('CallbackPage', () => {
  let component: CallbackPage;
  let fixture: ComponentFixture<CallbackPage>;
  const tokenSpy = jasmine.createSpyObj('TokenService', ['processCode']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CallbackPage],
      imports: [RouterTestingModule],
      providers: [{ provide: TokenService, useValue: tokenSpy }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(CallbackPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
