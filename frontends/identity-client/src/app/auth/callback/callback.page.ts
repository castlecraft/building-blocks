import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TokenService } from '../token/token.service';

@Component({
  selector: 'app-callback',
  templateUrl: './callback.page.html',
  styleUrls: ['./callback.page.css'],
})
export class CallbackPage implements OnInit {
  constructor(
    private readonly token: TokenService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      if (params.code && params.state) {
        const url = location.href;
        Promise.resolve(this.token.processCode(url))
          .then(() => this.router.navigate(['/home']))
          .then(success => {})
          .catch(error => {});
      }
    });
  }
}
