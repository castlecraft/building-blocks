export interface OAuth2Config {
  clientId?: string;
  profileURL?: string;
  tokenURL?: string;
  authServerURL?: string;
  authorizationURL?: string;
  revocationURL?: string;
  scope?: string;
  callbackURL?: string;
  isAuthRequiredToRevoke?: boolean;
  enableChoosingAccount?: boolean;
}

export interface AppInfoResponse {
  appURL?: string;
  scope: string;
  authServerURL?: string;
  authorizationURL?: string;
  callbackProtocol?: string;
  callbackURLs?: string[];
  callbackURL?: string;
  clientId?: string;
  communication?: boolean;
  introspectionURL?: string;
  profileURL?: string;
  revocationURL?: string;
  service?: string;
  services?: { type: string; url: string }[];
  tokenURL?: string;
  uuid?: string;
  enableChoosingAccount?: boolean;
}
