import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, from, Observable, of, throwError } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import * as sjcl from 'sjcl';
import {
  CLIENT_ID,
  REDIRECT_URI,
  SILENT_REFRESH_REDIRECT_URI,
  LOGIN_URL,
  ISSUER_URL,
  APP_URL,
  ENABLE_CHOOSING_ACCOUNT,
} from '../../constants/storage';

import { StorageService } from '../storage/storage.service';
import { BearerToken } from './bearer-token.interface';
import {
  ACCESS_TOKEN,
  APP_X_WWW_FORM_URLENCODED,
  AUTHORIZATION,
  BEARER_HEADER_PREFIX,
  CODE_VERIFIER,
  CONTENT_TYPE,
  EXPIRES_IN,
  ID_TOKEN,
  LOGGED_IN,
  ONE_HOUR_IN_SECONDS_NUMBER,
  REFRESH_TOKEN,
  STATE,
  TEN_MINUTES_IN_SECONDS_NUMBER,
} from './constants';
import { AppInfoResponse, OAuth2Config } from './credentials-config';
import { Browser } from './token.utils';

@Injectable({ providedIn: 'root' })
export class TokenService {
  private iab = new Browser();
  messageUrl = '/info'; // URL to web api

  config: Observable<OAuth2Config> = this.http
    .get<AppInfoResponse>(this.messageUrl)
    .pipe(
      switchMap(appInfo => {
        return this.http
          .get<AppInfoResponse>(appInfo.authServerURL + '/info')
          .pipe(
            map(authInfo => {
              appInfo.scope = 'openid email profile roles phone';
              appInfo.services = authInfo.services;
              appInfo.enableChoosingAccount = authInfo.enableChoosingAccount;
              appInfo.communication = authInfo.communication;
              if (appInfo?.callbackURLs?.length) {
                appInfo.callbackURL = appInfo.callbackURLs[0];
              }
              this.storeConfigValues(appInfo);
              return appInfo;
            }),
          );
      }),
    );

  private headers = {
    [CONTENT_TYPE]: APP_X_WWW_FORM_URLENCODED,
  };

  constructor(
    private http: HttpClient,
    private store: StorageService,
  ) {}

  storeConfigValues(response: AppInfoResponse) {
    this.store.setItem(CLIENT_ID, response.clientId);
    this.store.setItem(REDIRECT_URI, response.callbackURLs[0]);
    this.store.setItem(SILENT_REFRESH_REDIRECT_URI, response.callbackURLs[1]);
    this.store.setItem(LOGIN_URL, response.authorizationURL);
    this.store.setItem(ISSUER_URL, response.authServerURL);
    this.store.setItem(APP_URL, response.appURL);
    this.store.setItem(
      ENABLE_CHOOSING_ACCOUNT,
      JSON.stringify(response.enableChoosingAccount),
    );
  }

  logIn() {
    this.generateAuthUrl().subscribe({
      next: url => {
        this.iab.open(url, '_self');
      },
    });
  }

  logOut() {
    return this.store
      .getItem(ACCESS_TOKEN)
      .then(token => this.revokeToken(token, true))
      .then(() => this.store.clear())
      .then(() => this.store.setItem(LOGGED_IN, 'false'))
      .then(loggedOut => {})
      .catch(fail => {});
  }

  processCode(url: string) {
    const urlParts = new URL(url);
    const query = new URLSearchParams(urlParts.searchParams);
    const code = query.get('code') as string;
    if (!code) {
      return;
    }

    const error = query.get('error');
    if (error) {
      return;
    }

    const state = query.get('state') as string;
    forkJoin({
      savedState: from(this.store.getItem(STATE)),
      codeVerifier: from(this.store.getItem(CODE_VERIFIER)),
      config: this.config,
    })
      .pipe(
        switchMap(({ savedState, codeVerifier, config }) => {
          if (savedState !== state) {
            return of({ ErrorInvalidState: true });
          }
          const req = {
            grant_type: 'authorization_code',
            code,
            redirect_uri: config.callbackURL,
            client_id: config.clientId,
            scope: config.scope,
            code_verifier: codeVerifier,
          };

          return this.http.post<BearerToken>(
            config.tokenURL,
            new URLSearchParams(req).toString(),
            {
              headers: this.headers,
            },
          );
        }),
      )
      .subscribe({
        next: (response: BearerToken) => {
          const expiresIn = response.expires_in || ONE_HOUR_IN_SECONDS_NUMBER;
          const expirationTime = new Date();
          expirationTime.setSeconds(
            expirationTime.getSeconds() + Number(expiresIn),
          );

          this.store.setItem(ACCESS_TOKEN, response.access_token);

          this.saveRefreshToken(response.refresh_token);

          this.store.setItem(EXPIRES_IN, expirationTime.toISOString());
          this.store.setItem(ID_TOKEN, response.id_token);
          this.store.setItem(LOGGED_IN, 'true');
          this.store.removeItem(STATE);
          this.store.removeItem(CODE_VERIFIER);
        },
        error: err => {
          // eslint-disable-next-line no-console
          console.error({
            timestamp: new Date().toISOString(),
            ...err,
          });
        },
      });
  }

  revokeToken(accessToken: string, refresh: boolean = false) {
    forkJoin({
      config: this.config,
      token: from(this.store.getItem(ACCESS_TOKEN)),
    })
      .pipe(
        switchMap(({ config, token }) => {
          if (config.isAuthRequiredToRevoke) {
            this.headers[AUTHORIZATION] = BEARER_HEADER_PREFIX + accessToken;
          }
          return this.http.post(
            config.revocationURL,
            new URLSearchParams({ token }).toString(),
            {
              headers: { ...this.headers },
            },
          );
        }),
      )
      .subscribe({
        next: success => {
          if (refresh) {
            this.store.setItem(LOGGED_IN, 'false');
          }
        },
        error: error => {},
      });
  }

  getToken() {
    return forkJoin({
      expiration: from(this.store.getItem(EXPIRES_IN)),
      accessToken: from(this.store.getItem(ACCESS_TOKEN)),
    }).pipe(
      switchMap(({ expiration, accessToken }) => {
        if (!expiration) {
          return throwError(() => ({ InvalidExpiration: { expiration } }));
        }

        const now = new Date();
        const expirationTime = new Date(expiration);

        // expire 10 min early
        expirationTime.setSeconds(
          expirationTime.getSeconds() - TEN_MINUTES_IN_SECONDS_NUMBER,
        );
        if (now < expirationTime) {
          return of(accessToken);
        }
        return this.refreshToken();
      }),
    );
  }

  refreshToken() {
    return this.config.pipe(
      switchMap(config => {
        return from(this.getRefreshToken()).pipe(
          switchMap(refreshToken => {
            const requestBody = {
              grant_type: 'refresh_token',
              refresh_token: refreshToken,
              redirect_uri: config.callbackURL,
              client_id: config.clientId,
              scope: config.scope,
            };
            return this.http
              .post<BearerToken>(
                config.tokenURL,
                new URLSearchParams(requestBody).toString(),
                {
                  headers: this.headers,
                },
              )
              .pipe(
                switchMap(bearerToken => {
                  this.revokeToken(bearerToken.access_token);
                  const expirationTime = new Date();
                  const expiresIn =
                    bearerToken.expires_in || ONE_HOUR_IN_SECONDS_NUMBER;
                  expirationTime.setSeconds(
                    expirationTime.getSeconds() + Number(expiresIn),
                  );
                  this.store.setItem(EXPIRES_IN, expirationTime.toISOString());
                  this.store.setItem(ID_TOKEN, bearerToken.id_token);
                  this.store.setItem(ACCESS_TOKEN, bearerToken.access_token);

                  this.saveRefreshToken(bearerToken.refresh_token);
                  return of(bearerToken.access_token);
                }),
              );
          }),
        );
      }),
    );
  }

  generateAuthUrl() {
    return this.config.pipe(
      switchMap(config => {
        const state = this.generateRandomString();
        this.store.setItem(STATE, state);

        const codeVerifier = this.generateRandomString();
        this.store.setItem(CODE_VERIFIER, codeVerifier);

        const challenge = sjcl.codec.base64
          .fromBits(sjcl.hash.sha256.hash(codeVerifier))
          .replace(/\+/g, '-')
          .replace(/\//g, '_')
          .replace(/=/g, '');

        let url = config.authorizationURL;
        url += '?scope=' + encodeURIComponent(config.scope);
        url += '&response_type=code';
        url += '&client_id=' + config.clientId;
        url += '&redirect_uri=' + encodeURIComponent(config.callbackURL);
        url += '&state=' + state;
        url += '&code_challenge_method=S256';
        url += '&prompt=select_account';
        url += '&code_challenge=' + challenge;

        return of(url);
      }),
    );
  }

  generateRandomString(stateLength: number = 32) {
    let result = '';
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < stateLength; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  saveRefreshToken(refreshToken: string) {
    this.store
      .setItem(REFRESH_TOKEN, refreshToken)
      .then(success => {})
      .catch(fail => {});
  }

  getRefreshToken(): Promise<string> {
    return this.store.getItem(REFRESH_TOKEN);
  }

  loadProfile() {
    return this.config.pipe(
      switchMap(config => {
        return this.getToken().pipe(
          switchMap(token => {
            return this.http.get<any>(config.profileURL, {
              headers: { authorization: 'Bearer ' + token },
            });
          }),
        );
      }),
    );
  }
}
