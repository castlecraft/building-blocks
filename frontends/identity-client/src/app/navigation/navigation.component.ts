import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { forkJoin, from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NavigationService } from './navigation.service';
import {
  ISSUER_URL,
  APP_URL,
  ENABLE_CHOOSING_ACCOUNT,
  USER_UUID,
} from '../constants/storage';
import { LOGOUT_URL } from '../constants/url-paths';
import { ProfileComponent } from '../profile/profile.component';
import { TokenService } from '../auth/token/token.service';
import { LOGGED_IN } from '../auth/token/constants';
import { SET_ITEM, StorageService } from '../auth/storage/storage.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
})
export class NavigationComponent implements OnInit {
  enableChoosingAccount = false;
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));
  tokenIsValid: boolean;
  avatar: string;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private navigationService: NavigationService,
    private token: TokenService,
    private store: StorageService,
  ) {}

  ngOnInit(): void {
    this.store.getItem(LOGGED_IN).then(loggedIn => {
      if (loggedIn === 'true') {
        this.tokenIsValid = true;
        this.router.navigate(['/profile']);
      }
    });

    this.store.getItem(ENABLE_CHOOSING_ACCOUNT).then(enableChoosingAccount => {
      if (enableChoosingAccount === 'true') {
        this.enableChoosingAccount = true;
      }
    });

    this.store.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === LOGGED_IN) {
          if (res.value?.value === 'true') {
            this.tokenIsValid = true;
            this.router.navigate(['/profile']);
          }
        }
      },
      error: error => {},
    });

    this.setAutoLoginInDuration();
  }

  setAutoLoginInDuration() {
    this.navigationService.setAutoLoginInDuration();
  }

  onActivate(profileComponent: ProfileComponent) {
    if (profileComponent && profileComponent.messageEvent) {
      profileComponent.messageEvent.subscribe({
        next: avatar => (this.avatar = avatar),
        error: error => {},
      });
    }
  }

  login() {
    this.token.logIn();
  }

  logout() {
    forkJoin({
      issuerURL: from(this.store.getItem(ISSUER_URL)),
      appURL: from(this.store.getItem(APP_URL)),
    }).subscribe({
      next: ({ issuerURL, appURL }) => {
        const logoutUrl = issuerURL + '/auth/logout?redirect=' + appURL;
        this.navigationService.clearInfoStorage();
        this.token.logOut();
        this.tokenIsValid = false;
        window.location.href = logoutUrl;
      },
      error: error => {},
    });
  }

  chooseAccount() {
    this.store.getItem(APP_URL).then(appURL => {
      window.open(appURL, '_blank', 'noreferrer=true');
    });
  }

  logoutCurrentUser() {
    forkJoin({
      issuerURL: from(this.store.getItem(ISSUER_URL)),
      userUUID: from(this.store.getItem(USER_UUID)),
      appURL: from(this.store.getItem(APP_URL)),
    }).subscribe({
      next: ({ issuerURL, userUUID, appURL }) => {
        const logoutURL =
          issuerURL +
          LOGOUT_URL +
          '/' +
          userUUID +
          '?redirect=' +
          encodeURIComponent(appURL);
        this.store.clear().then(() => {
          window.location.href = logoutURL;
        });
      },
      error: error => {},
    });
  }
}
