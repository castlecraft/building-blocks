import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  CLIENT_ID,
  REDIRECT_URI,
  SILENT_REFRESH_REDIRECT_URI,
  LOGIN_URL,
  ISSUER_URL,
  USER_UUID,
} from '../constants/storage';
import { GET_PROFILE_DETAILS_URL } from '../constants/url-paths';
import { switchMap, timer } from 'rxjs';
import {
  AUTHORIZATION,
  BEARER_HEADER_PREFIX,
  LOGGED_IN,
} from '../auth/token/constants';
import { TokenService } from '../auth/token/token.service';
import { StorageService } from '../auth/storage/storage.service';
import { DURATION } from '../constants/app-constants';

@Injectable()
export class NavigationService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
    private readonly store: StorageService,
  ) {}

  setAutoLoginInDuration() {
    timer(DURATION).subscribe(() => {
      this.store.getItem(LOGGED_IN).then(loggedIn => {
        if (loggedIn !== 'true') {
          this.token.logIn();
        }
      });
    });
  }

  clearInfoStorage() {
    this.store.removeItem(CLIENT_ID);
    this.store.removeItem(REDIRECT_URI);
    this.store.removeItem(SILENT_REFRESH_REDIRECT_URI);
    this.store.removeItem(LOGIN_URL);
    this.store.removeItem(ISSUER_URL);
    this.store.removeItem(USER_UUID);
  }

  getAvatar(uuid: string) {
    return this.token.getToken().pipe(
      switchMap(token => {
        return this.http.get(GET_PROFILE_DETAILS_URL + '/' + uuid, {
          headers: {
            [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
          },
        });
      }),
    );
  }
}
