import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { from } from 'rxjs';
import { Component } from '@angular/core';

@Component({ selector: 'app-navigation', template: '' })
class NavigationComponent {}

describe('AppComponent', () => {
  const appServiceStub: Partial<AppService> = {
    getMessage: () => {
      return from([]);
    },
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [AppComponent, NavigationComponent],
      providers: [
        {
          provide: AppService,
          useValue: appServiceStub,
        },
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeDefined();
  }));
});
