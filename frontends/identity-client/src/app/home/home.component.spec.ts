import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Component } from '@angular/core';

import { HomeComponent } from './home.component';
import { TokenService } from '../auth/token/token.service';
import { of } from 'rxjs';

@Component({ selector: 'app-profile', template: '' })
class ProfileComponent {}

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  const tokenServiceStub: Partial<TokenService> = {
    getToken() {
      return of('access_token');
    },
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [HomeComponent, ProfileComponent],
      imports: [
        RouterTestingModule.withRoutes([
          { path: 'profile', component: ProfileComponent },
        ]),
      ],
      providers: [
        {
          provide: TokenService,
          useValue: tokenServiceStub,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
