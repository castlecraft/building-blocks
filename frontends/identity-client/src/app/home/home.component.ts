import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { HOME_TITLE } from '../constants/messages';
import { TokenService } from '../auth/token/token.service';
import { map } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  constructor(
    private title: Title,
    private token: TokenService,
    private router: Router,
  ) {}

  ngOnInit() {
    this.title.setTitle(HOME_TITLE);
    this.token.getToken().pipe(
      map(token => {
        if (token) {
          this.router.navigate(['/profile']);
          return true;
        }
        return false;
      }),
    );
  }
}
