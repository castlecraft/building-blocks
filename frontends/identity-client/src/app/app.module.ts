import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { LayoutModule } from '@angular/cdk/layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { HttpErrorHandler } from './common/http-error-handler.service';
import { MessageService } from './common/message.service';
import { NavigationComponent } from './navigation/navigation.component';
import { MaterialModule } from './material.module';
import { AuthGuard } from './guards/auth.guard.service';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { AppsComponent } from './apps/apps.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NavigationService } from './navigation/navigation.service';
import { ProfileService } from './profile/profile.service';
import { MultifactorComponent } from './multifactor/multifactor.component';
import { PasswordRequirementComponent } from './password-requirement/password-requirement.component';
import { UpdatePhoneComponent } from './update-phone/update-phone.component';
import { UpdatePhoneService } from './update-phone/update-phone.service';
import { UpdateEmailComponent } from './update-email/update-email.component';
import { CallbackPage } from './auth/callback/callback.page';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HomeComponent,
    ProfileComponent,
    AppsComponent,
    MultifactorComponent,
    PasswordRequirementComponent,
    UpdatePhoneComponent,
    UpdateEmailComponent,
    CallbackPage,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  providers: [
    AppService,
    HttpErrorHandler,
    MessageService,
    AuthGuard,
    Title,
    ProfileService,
    NavigationService,
    UpdatePhoneService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
