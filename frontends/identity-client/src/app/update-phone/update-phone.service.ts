import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ISSUER_URL } from '../constants/storage';
import { ADD_UNVERIFIED_PHONE, VERIFY_PHONE } from '../constants/url-paths';
import { forkJoin, from, switchMap } from 'rxjs';
import { TokenService } from '../auth/token/token.service';
import { StorageService } from '../auth/storage/storage.service';
import { AUTHORIZATION, BEARER_HEADER_PREFIX } from '../auth/token/constants';

@Injectable({
  providedIn: 'root',
})
export class UpdatePhoneService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
    private readonly store: StorageService,
  ) {}

  addUnverifiedPhone(unverifiedPhone: string) {
    return forkJoin({
      issuerURL: from(this.store.getItem(ISSUER_URL)),
      token: this.token.getToken(),
    }).pipe(
      switchMap(({ issuerURL, token }) => {
        const url = issuerURL + ADD_UNVERIFIED_PHONE;
        const headers = new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set(AUTHORIZATION, BEARER_HEADER_PREFIX + token);
        return this.http.post(url, { unverifiedPhone }, { headers });
      }),
    );
  }

  verifyPhone(otp: string) {
    return forkJoin({
      issuerURL: from(this.store.getItem(ISSUER_URL)),
      token: this.token.getToken(),
    }).pipe(
      switchMap(({ issuerURL, token }) => {
        const url = issuerURL + VERIFY_PHONE;
        const headers = new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set(AUTHORIZATION, BEARER_HEADER_PREFIX + token);
        return this.http.post(url, { otp }, { headers });
      }),
    );
  }
}
