import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { forkJoin, from, throwError } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import {
  UPDATE_PERSONAL_DETAILS_URL,
  UPDATE_PROFILE_DETAILS_URL,
  GET_PERSONAL_DETAILS_URL,
  GET_PROFILE_DETAILS_URL,
  GET_AUTH_SERVER_USER,
  SET_AUTH_SERVER_USER,
  CHANGE_PASSWORD_ENDPOINT,
  DELETE_AVATAR_ENDPOINT,
  DELETE_ME_ENDPOINT,
  FORGOT_PASSWORD,
  USER_INFO,
  INFO_ENDPOINT,
  EMAIL_VERIFICATION_CODE_ENDPOINT,
  ADD_UNVERIFIED_EMAIL_ENDPOINT,
  AVATAR_UPLOAD_ENDPOINT,
  DISABLE_PASSWORD_LESS_LOGIN_ENDPOINT,
  ENABLE_PASSWORD_LESS_LOGIN_ENDPOINT,
} from '../constants/url-paths';
import { ISSUER_URL, APP_URL } from '../constants/storage';
import { CLOSE, CURRENT_PASSWORD_MISMATCH } from '../constants/messages';
import { NavigationService } from '../navigation/navigation.service';
import { DURATION } from '../constants/app-constants';
import { IDTokenClaims } from './id-token-claims.interfaces';
import { TokenService } from '../auth/token/token.service';
import { AUTHORIZATION, BEARER_HEADER_PREFIX } from '../auth/token/constants';
import { StorageService } from '../auth/storage/storage.service';

@Injectable()
export class ProfileService {
  constructor(
    private http: HttpClient,
    private token: TokenService,
    private store: StorageService,
    private snackBar: MatSnackBar,
    private navigationService: NavigationService,
  ) {}

  updatePersonalDetails(
    uuid: string,
    givenName: string,
    middleName: string,
    familyName: string,
    nickname: string,
    gender: string,
    birthdate: string,
  ) {
    return this.token.getToken().pipe(
      switchMap(token => {
        return this.http.post(
          UPDATE_PERSONAL_DETAILS_URL,
          {
            uuid,
            givenName,
            middleName,
            familyName,
            nickname,
            gender,
            birthdate,
          },
          {
            headers: {
              [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
            },
          },
        );
      }),
    );
  }

  updateProfileDetails(
    uuid: string,
    website: string,
    zoneinfo: string,
    locale: string,
  ) {
    return this.token.getToken().pipe(
      switchMap(token => {
        return this.http.post(
          UPDATE_PROFILE_DETAILS_URL,
          { uuid, website, zoneinfo, locale },
          {
            headers: {
              [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
            },
          },
        );
      }),
    );
  }

  disablePasswordLess() {
    return forkJoin({
      issuerURL: from(this.store.getItem(ISSUER_URL)),
      token: this.token.getToken(),
    }).pipe(
      switchMap(({ issuerURL, token }) => {
        const url = issuerURL + DISABLE_PASSWORD_LESS_LOGIN_ENDPOINT;
        return this.http.post(
          url,
          {},
          {
            headers: {
              [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
            },
          },
        );
      }),
    );
  }

  enablePasswordLess() {
    return forkJoin({
      issuerURL: from(this.store.getItem(ISSUER_URL)),
      token: this.token.getToken(),
    }).pipe(
      switchMap(({ issuerURL, token }) => {
        const url = issuerURL + ENABLE_PASSWORD_LESS_LOGIN_ENDPOINT;
        return this.http.post(
          url,
          {},
          {
            headers: {
              [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
            },
          },
        );
      }),
    );
  }

  getPersonalDetails(uuid) {
    return this.token.getToken().pipe(
      switchMap(token => {
        return this.http.get(GET_PERSONAL_DETAILS_URL + '/' + uuid, {
          headers: {
            [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
          },
        });
      }),
    );
  }

  getProfileDetails(uuid) {
    return this.token.getToken().pipe(
      switchMap(token => {
        return this.http.get(GET_PROFILE_DETAILS_URL + '/' + uuid, {
          headers: {
            [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
          },
        });
      }),
    );
  }

  getAuthServerUser() {
    return forkJoin({
      issuerURL: from(this.store.getItem(ISSUER_URL)),
      token: this.token.getToken(),
    }).pipe(
      switchMap(({ issuerURL, token }) => {
        const url = issuerURL + GET_AUTH_SERVER_USER;
        return this.http.get(url, {
          headers: {
            [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
          },
        });
      }),
    );
  }

  getOIDCProfile() {
    return forkJoin({
      issuerURL: from(this.store.getItem(ISSUER_URL)),
      token: this.token.getToken(),
    }).pipe(
      switchMap(({ issuerURL, token }) => {
        const url = issuerURL + USER_INFO;
        return this.http.get<IDTokenClaims>(url, {
          headers: {
            [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
          },
        });
      }),
    );
  }

  uploadAvatar(selectedFile) {
    const uploadData = new FormData();
    uploadData.append('file', selectedFile, selectedFile.name);
    return this.token.getToken().pipe(
      switchMap(token => {
        return this.http.post(AVATAR_UPLOAD_ENDPOINT, uploadData, {
          reportProgress: true,
          headers: {
            [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
          },
        });
      }),
    );
  }

  setAuthServerUser(user) {
    return forkJoin({
      issuerURL: from(this.store.getItem(ISSUER_URL)),
      token: this.token.getToken(),
    }).pipe(
      switchMap(({ issuerURL, token }) => {
        return this.http.post(issuerURL + SET_AUTH_SERVER_USER, user, {
          headers: {
            [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
          },
        });
      }),
    );
  }

  changePassword(
    currentPassword: string,
    newPassword: string,
    repeatPassword: string,
  ) {
    if (newPassword !== repeatPassword) {
      this.snackBar.open(CURRENT_PASSWORD_MISMATCH, CLOSE, {
        duration: DURATION,
      });
      return throwError(() => ({ message: CURRENT_PASSWORD_MISMATCH }));
    } else {
      return forkJoin({
        issuerURL: from(this.store.getItem(ISSUER_URL)),
        token: this.token.getToken(),
      }).pipe(
        switchMap(({ issuerURL, token }) => {
          return this.http.post(
            issuerURL + CHANGE_PASSWORD_ENDPOINT,
            {
              currentPassword,
              newPassword,
            },
            {
              headers: {
                [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
              },
            },
          );
        }),
      );
    }
  }

  logout() {
    this.navigationService.clearInfoStorage();
  }

  deleteAvatar() {
    return forkJoin({
      appURL: from(this.store.getItem(APP_URL)),
      token: this.token.getToken(),
    }).pipe(
      switchMap(({ appURL, token }) => {
        return this.http.post(
          appURL + DELETE_AVATAR_ENDPOINT,
          {},
          {
            headers: {
              [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
            },
          },
        );
      }),
    );
  }

  deleteUser() {
    return forkJoin({
      issuerURL: from(this.store.getItem(ISSUER_URL)),
      token: this.token.getToken(),
    }).pipe(
      switchMap(({ issuerURL, token }) => {
        return this.http.post(
          issuerURL + DELETE_ME_ENDPOINT,
          {},
          {
            headers: {
              [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
            },
          },
        );
      }),
    );
  }

  setPassword() {
    return forkJoin({
      issuerURL: from(this.store.getItem(ISSUER_URL)),
      token: this.token.getToken(),
    }).pipe(
      switchMap(({ issuerURL, token }) => {
        return this.http
          .get<{
            email?: string;
          }>(issuerURL + USER_INFO, {
            headers: {
              [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
            },
          })
          .pipe(
            switchMap(userInfo => {
              return this.http.post(issuerURL + FORGOT_PASSWORD, {
                emailOrPhone: userInfo.email,
              });
            }),
          );
      }),
    );
  }

  checkServerForPhoneRegistration() {
    return from(this.store.getItem(ISSUER_URL)).pipe(
      switchMap(issuerURL => {
        return this.http.get<any>(issuerURL + INFO_ENDPOINT);
      }),
    );
  }

  emailVerificationCode() {
    return forkJoin({
      issuerURL: from(this.store.getItem(ISSUER_URL)),
      token: this.token.getToken(),
    }).pipe(
      switchMap(({ issuerURL, token }) => {
        return this.http.post(
          issuerURL + EMAIL_VERIFICATION_CODE_ENDPOINT,
          undefined,
          {
            headers: {
              [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
            },
          },
        );
      }),
    );
  }

  updateEmail(email: string) {
    return forkJoin({
      issuerURL: from(this.store.getItem(ISSUER_URL)),
      token: this.token.getToken(),
    }).pipe(
      switchMap(({ issuerURL, token }) => {
        return this.http.post(
          issuerURL + ADD_UNVERIFIED_EMAIL_ENDPOINT,
          { unverifiedEmail: email },
          {
            headers: {
              [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
            },
          },
        );
      }),
    );
  }
}
