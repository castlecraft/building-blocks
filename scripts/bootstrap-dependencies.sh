#!/bin/bash

# Script needs to be executed from
# building-blocks lerna / monorepo root
rm -fr node_modules && yarn
rm -fr apps/**/node_modules && rm -fr frontends/**/node_modules
yarn --cwd apps/authorization-server --ignore-scripts
yarn --cwd apps/communication-server --ignore-scripts
yarn --cwd apps/identity-provider --ignore-scripts
yarn --cwd apps/infrastructure-console --ignore-scripts
yarn --cwd frontends/admin-client --ignore-scripts
yarn --cwd frontends/authorization-client --ignore-scripts
yarn --cwd frontends/identity-client --ignore-scripts
