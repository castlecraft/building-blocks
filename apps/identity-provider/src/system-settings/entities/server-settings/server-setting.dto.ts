import { IsUrl, IsNotEmpty, IsUUID, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ServerSettingsDto {
  uuid?: string;

  @IsUrl({ require_tld: false })
  @ApiProperty({
    description: 'The URL of the server.',
    type: 'string',
    required: true,
  })
  appURL: string;

  @IsUrl({ require_tld: false })
  authServerURL: string;

  @IsNotEmpty()
  clientId: string;

  @IsNotEmpty()
  clientSecret: string;

  @IsUrl({ allow_underscores: true, require_tld: false }, { each: true })
  @IsOptional()
  callbackURLs: string[];

  @IsUUID()
  @IsOptional()
  cloudStorageSettings: string;
}
