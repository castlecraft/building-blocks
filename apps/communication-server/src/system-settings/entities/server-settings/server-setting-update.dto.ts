import { IsUrl, IsNotEmpty, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ServerSettingsUpdateDto {
  @IsOptional()
  @IsUrl({ require_tld: false })
  @ApiProperty({
    description: 'The URL of the server.',
    type: 'string',
    required: true,
  })
  appURL: string;

  @IsOptional()
  @IsUrl({ require_tld: false })
  authServerURL: string;

  @IsOptional()
  @IsNotEmpty()
  clientId: string;

  @IsOptional()
  @IsNotEmpty()
  clientSecret: string;

  @IsOptional()
  @IsUrl({ allow_underscores: true, require_tld: false }, { each: true })
  callbackURLs: string[];

  @IsOptional()
  communicationServerSystemEmailAccount: string;
}
