import { Injectable } from '@nestjs/common';
import {
  HealthIndicator,
  HealthIndicatorResult,
  HealthCheckError,
} from '@nestjs/terminus';
import mongoose, { STATES } from 'mongoose';

@Injectable()
export class DatabaseHealthIndicatorService extends HealthIndicator {
  async isHealthy(): Promise<HealthIndicatorResult> {
    const isHealthy = mongoose.connection.readyState === STATES.connected;
    const result = this.getStatus('database', isHealthy);

    if (isHealthy) {
      return result;
    }

    throw new HealthCheckError(this.constructor.name, result);
  }
}
