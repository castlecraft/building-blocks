import { IsEmail, IsNotEmpty, IsOptional, IsUrl } from 'class-validator';

export class SignupViaEmailDto {
  @IsNotEmpty()
  name: string;
  @IsEmail()
  email: string;
  @IsUrl({ require_tld: false })
  @IsOptional()
  redirect: string;
}
