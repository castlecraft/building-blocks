import { Entity, BaseEntity, Column, ObjectId, ObjectIdColumn } from 'typeorm';

@Entity()
export class ServiceType extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectId;

  @Column()
  uuid: string;

  @Column()
  name: string;
}
