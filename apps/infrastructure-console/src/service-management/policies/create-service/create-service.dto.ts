import { IsString, IsNotEmpty, IsUUID, IsUrl } from 'class-validator';

export class CreateServiceDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsUUID()
  clientId: string;

  @IsUrl({ allow_underscores: true, require_tld: false })
  serviceURL: string;

  @IsString()
  @IsNotEmpty()
  type: string;
}
